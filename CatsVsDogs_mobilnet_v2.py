#!/usr/bin/env python
# coding: utf-8

# In[6]:


#Library
import numpy as np
import pandas as pd 
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import random
import os
import tensorflow_hub as hub
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator


# In[7]:


#Parameters for the imput images
IMAGE_WIDTH=224
IMAGE_HEIGHT=224
IMAGE_SIZE=(IMAGE_WIDTH, IMAGE_HEIGHT)
IMAGE_CHANNELS=3 #3Channels(RGB)
#Images Directory
train_dir = 'C:\\Users\\steve\\Desktop\\cat_dog\\train\\train\\'
test_dir = 'C:\\Users\\steve\\Desktop\\cat_dog\\test1\\test1\\'


# In[12]:


#Preparation of the dataframe
filenames = os.listdir(train_dir)
categories = []
for filename in filenames:
    category = filename.split('.')[0] #this allows to split the data in cat and dog category since the image are named"Dog.1.jpg"  
    if category == 'dog':
        categories.append('dog')
    else:
        categories.append('cat')

df = pd.DataFrame({ #creation of the dataframe
    'filename': filenames,
    'category': categories
})
print(df)


# In[9]:


#model import + creation of the last layer specific for our data

URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4" #URL of the pre-trained model

mobile_net = hub.KerasLayer(URL, input_shape=(IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS)) #hub allow to import pre-trained model// input_shape for mobilnet_v2 need to be 224x224x3
mobile_net.trainable = False

model = tf.keras.models.Sequential([ #create an output from the previous pre-trained layers
    mobile_net,
    tf.keras.layers.Dense(2, activation='softmax') # softmax converts a real vector to a vector of categorical probabilities.
                                                   # The elements of the output vector are in range (0, 1) and sum to 1.
    ])

model.compile(optimizer='adam', # optimizer adam : efficient on calculations and requiring little memory
              loss='categorical_crossentropy', #Computes the crossentropy loss between the labels and predictions (used for unique categorization)
              metrics=['accuracy']) # Calculates how often predictions equals labels.

model.summary() #summary of the final model with our last layer


# In[10]:


from keras.callbacks import EarlyStopping, ReduceLROnPlateau
earlystop = EarlyStopping(patience=1) #stop the training if the val_accuracy doesn't improve during 1 epoch
learning_reduction = ReduceLROnPlateau(monitor='val_accuracy', #Reduce learning rate when the metric 'val_accuracy' has stopped improving.
                                            patience=2, #patience of 2 epoch before reducing 
                                            verbose=1,  #Show an animated bar of the training progress
                                            factor=0.5, #Reduction factor
                                            min_lr=0.00001)#minimum learning rate
callbacks = [earlystop, learning_reduction] #the object callbacks allows to perform actions at various stages of training


# In[24]:


#Preparation of the data
#split of the train data into train data + validation data
train_df, validate_df = train_test_split(df, test_size=0.20, random_state=42) #split the dataset 80% of image for training and 20% to validate

train_df = train_df.reset_index(drop=True) #Generate new dataframe with a reset of the index
validate_df = validate_df.reset_index(drop=True) #Generate new dataframe with a reset of the index

total_train = train_df.shape[0] # return the dimensions of the rows and colunms
total_validate = validate_df.shape[0] # return the dimensions of the rows and colunms

batch_size= 5 # defines the number of samples that will be propagated through the network. Train faster


# In[18]:


train_datagen = ImageDataGenerator( #Generate batches of tensor image data with real-time data augmentation
    rotation_range=20, #Degree range for random rotations
    rescale=1./255, #rescaling factor
    shear_range=0.1, #Shear angle in counter-clockwise direction in degrees
    zoom_range=0.25, #Range for random zoom
    horizontal_flip=True, #Randomly flip inputs horizontally
    width_shift_range=0.1, #Float, 1-D array-like or in
    height_shift_range=0.1 #Float, 1-D array-like or in
)

train_generator = train_datagen.flow_from_dataframe( #Takes the dataframe and the path to a directory + generates batches
    train_df, #dataframe for the training 
    train_dir, #path to the directory to read images from
    x_col='filename', #column in dataframe that contains the filenames
    y_col='category', #column in dataframe that has the target data.
    target_size=IMAGE_SIZE, # The dimensions to which all images found will be resized
    class_mode='categorical', #2D numpy array of one-hot encoded labels. Supports multi-label output
    batch_size=batch_size #size of the batches of data 
)


# In[19]:


validation_datagen = ImageDataGenerator(rescale=1./255)
validation_generator = validation_datagen.flow_from_dataframe(
    validate_df, 
    train_dir, 
    x_col='filename',
    y_col='category',
    target_size=IMAGE_SIZE,
    class_mode='categorical',
    batch_size=batch_size
)


# In[19]:


epochs=3 #number of times we go through the training set
CatDog = model.fit_generator( #function that allows to train the model
    train_generator, #data for the training
    epochs=epochs, #number of epochs
    validation_data = validation_generator, #data for the validation
    validation_steps = total_validate//batch_size, #number of steps required to perform images validation
    steps_per_epoch= total_train//batch_size, #number of steps required to perform images training
    callbacks=callbacks #allows us to use the callback that we created before
)
model.save("CatsVsDogs.h5") #saving the new trained model
print("model saved")


# In[25]:


accuracy = CatDog.history['accuracy']
val_accuracy = CatDog.history['val_accuracy']
loss = CatDog.history['loss']
val_loss = CatDog.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, accuracy, label='Training Accuracy')
plt.plot(epochs_range, val_accuracy, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()

