print("chargement des librairie")
from matplotlib import pyplot 
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.optimizers import SGD


# define cnn model
# VGG 3 blocs + data augmentation
def define_model():
	model = Sequential()
	model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(200, 200, 3)))
	model.add(MaxPooling2D((2, 2)))
	model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
	model.add(MaxPooling2D((2, 2)))
	model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
	model.add(MaxPooling2D((2, 2)))
	model.add(Flatten())
	model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
	model.add(Dense(1, activation='sigmoid'))
	# compile model
	opt = SGD(lr=0.001, momentum=0.9)
	model.compile(optimizer=opt, loss='binary_crossentropy', metrics=['accuracy'])
	return model


def summarize_diagnostics(history):
	#plot accuracy
 print("Realisation des Graphiques")
 pyplot.plot(history.history['accuracy'])
 pyplot.plot(history.history['val_accuracy'])
 pyplot.title('VGG 3 blocs + Data Augmentation Accuracy')
 pyplot.ylabel('accuracy')
 pyplot.xlabel('epoch')
 pyplot.legend(['train', 'val'], loc='center left')
 pyplot.savefig("accurancy_VGG_3b_data_augmentation.png")
 pyplot.show()
	
	#plot loss
 pyplot.plot(history.history['loss'])
 pyplot.plot(history.history['val_loss'])
 pyplot.title('VGG 3 blocs + Data Augmentation  loss')
 pyplot.ylabel('loss')
 pyplot.xlabel('epoch')
 pyplot.legend(['train', 'val'], loc='center left')
 pyplot.savefig("loss_VGG_3b_data_augmentation.png")
 pyplot.show()
# run the test harness for evaluating a model
def run_test_harness():
	# define model
	print("Creation du model")
	model = define_model()
	# create data generators
	jeu_entrainement = ImageDataGenerator(rescale=1.0/255.0,
		width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)
	jeu_test = ImageDataGenerator(rescale=1.0/255.0)
	# prepare iterators
	train_it = jeu_entrainement.flow_from_directory('dataset_dogs_vs_cats/train/',
		class_mode='binary', batch_size=64, target_size=(200, 200))
	test_it = jeu_test.flow_from_directory('dataset_dogs_vs_cats/test/',
		class_mode='binary', batch_size=64, target_size=(200, 200))
    # fit model
	history = model.fit_generator(train_it, steps_per_epoch=len(train_it),
		validation_data=test_it, validation_steps=len(test_it), epochs=50, verbose=0)
    
	# evaluate model
	_, acc = model.evaluate_generator(test_it, steps=len(test_it), verbose=0)
	print('> %.3f' % (acc * 100.0))
    
    
	# learning curves
    
	summarize_diagnostics(history)
    # save model
	model.save('final_model_CNN3b_DataAugmentation.h5')

    




# entry point, run the test harness
print("Execution fonction")
run_test_harness()

#accuracy 86.879