# Prédisposition
Nous travaillons sur python3
- Terminal Linux
```
sudo apt-get update
sudo apt-get install python3.6
```
- Terminal Mac os
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install python3
```
- Windows

[Installer python 3](https://phoenixnap.com/kb/how-to-install-python-3-windows)
# Mac os / Linux
### Installation des modules

1. Travaillez sur un environnement ou tensorflow est instalé ou l'instalé

```
    pip3 install "tensorflow_hub>=0.6.0" #permet l' utilisation de modele préenregistré
    pip3 install "tensorflow>=2.0.0"# permet l'utilisation de fonction de machine learning
    pip3 install pillow # permet la gestion des images et l'affichage

```
### Exécution du programme

Ouvrir un environnement contenant tensorflow 
2. Travaillez a l'endroit ou se situe votre dossier "dataset_dogs_vs_cats/"

```
    python3 Interface_Graphic_machine_learning.py

```
Si le programme n'a pas les droit `chmod u+x Interface_Graphic_machine_learning.py `

# Windows

Ouvrir un terminal windows :
1. Cliquer sur le logo windows 
2. Taper cmd puis entrer
3. Taper py puis entrer dans la console
   Si vous avez ce message :
> 'py' n'est pas reconnu en tant que commande interne ou externe, un programme exécutable ou un fichier de commandes. 


[Executer un script python sous windows](https://pythonfaqfr.readthedocs.io/en/latest/executer_script.html " executer un script python sous windows")

Si la commande et reconnue 

### Installation des modules
```
    pip3 install --user --upgrade tensorflow
    pip3 install --upgrade tensorflow-hub
    pip3 install pillow

```
### Exécution du programme

```
py Chemin\absolu\ou\situe\script\Interface_Graphic_machine_learning.py
```
[Liens d'aide execution script](https://docs.python.org/fr/3/faq/windows.html "execution un script aide")

On quitte la console en tapant exit() ou ctrl +Z