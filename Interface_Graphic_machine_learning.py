#!/usr/bin/python3

import tkinter as tk	
from tkinter import *
from tkinter import filedialog
from PIL import Image, ImageTk
import tkinter.font as tkFont	
import tensorflow as tf 
import tensorflow_hub as hub
import time
from tensorflow.keras.preprocessing.image import ImageDataGenerator

class MyWindow(Tk):
    # create on the main windows
    def __init__(self):
        
        Tk.__init__(self)
        #places the window in the center of the screen
        self.eval('tk::PlaceWindow %s center' % self.winfo_pathname(self.winfo_id()))
        #self.geometry( "600x700" )
        self.title( " Cat vs Dog " )
        # font family of the main windows
        fontStyle = tkFont.Font(family="Helvetica", size=20) 
        self.__Image = tk.StringVar()
        #image selection
        label = tk.Label( self, text="Veuillez choisir une image :\n",font=fontStyle)
        label.pack()

        #read a picture
        button_f = tk.Button(self,text = "Ouvrir une image",command = self.fileDialog) 
        button_f.pack()
        #model selection
        label2 = tk.Label( self, text="\n Choix du modele :",font=fontStyle)
        label2.pack()
        self.__CheckVar1 = tk.IntVar()
        self.__CheckVar2 = tk.IntVar()
        C1 = tk.Checkbutton(self, text = "VGG + Augmentation des donnees", variable = self.__CheckVar1, onvalue = 1, offvalue = 0, height=5, width = 32,font=fontStyle)
        C2 = tk.Checkbutton(self, text = "Transfert de l'apprentissage ", variable = self.__CheckVar2, onvalue = 1, offvalue = 0, height=5, width = 32,font=fontStyle)
        C1.pack()
        C2.pack()
        #accuracy /probability of the prediction
        label3 = tk.Label( self, text=" Voulez vous voir la precision/probabibilité \n ( le chargement des résultats sera plus lent) : \n",font=fontStyle)
        label3.pack()
        self.__var = tk.IntVar()
        R1 = tk.Radiobutton(master=self, text="Oui", variable=self.__var , value=1,font=fontStyle)
        R1.pack( anchor = tk.S )
        R2 = tk.Radiobutton(master=self, text="Non", variable=self.__var , value=2,font=fontStyle)
        R2.pack( anchor = tk.S )
    #submit
        bouton_q= tk.Button(self,text="Submit",bg="brown",fg="white",command=self.createNewWindow)
        bouton_q.pack()
        #quit the programs
        button_exit = tk.Button(master=self, text = " Sortir ", command = self.destroy)
        button_exit.pack() 
     #function with open the picture
    def fileDialog(self):
        self.__filename =  filedialog.askopenfilename(title="Ouvrir une image",filetypes=[('jpg files','.jpg'),('png files','.png'),('bmp files','.bmp'),('all files','.*')]) 
#Analysis windows the second windows
    def createNewWindow(self):
        fenetreResult=tk.Tk()
        #places the window in the center of the screen
        fenetreResult.eval('tk::PlaceWindow %s center' % fenetreResult.winfo_pathname(fenetreResult.winfo_id()))
        #title of the windows
        fenetreResult.title("Resultat Chien ou Chat ?")
        fontStyle_Result = tkFont.Font(family="Helvetica", size=20)
        #management of all types of images
        photo = ImageTk.PhotoImage(master=fenetreResult,file=self.__filename)
        #image display
        affich_im = tk.Label(master=fenetreResult,image=photo)
        affich_im.image = photo 
        affich_im.pack()
        # load model
        # PREDICTION WITH vgg 3 blocs and augmantation data
        if (self.__CheckVar1.get() == 1 and self.__CheckVar2.get()== 0) :
            #load model vgg 3 blocs
            model_b = tf.keras.models.load_model('final_model_CNN3b_DataAugmentation.h5')
            #load picture in the model
            img = tf.keras.preprocessing.image.load_img(self.__filename, target_size=(200, 200))
            # convert to array
            img = tf.keras.preprocessing.image.img_to_array(img)
            # reshape into a single sample with 3 channels
            img = img.reshape(1, 200, 200, 3)
            # center pixel data
            img = img.astype('float32')
            img = img - [123.68, 116.779, 103.939]
        # predict the class
            result = model_b.predict(img)
            # Analysis of the prediction
            if(result[0]==0):
                Chat=tk.Label(master=fenetreResult,text="C\'est un chat",font=fontStyle_Result)
                Chat.pack()
            else:
                Chien=tk.Label(master=fenetreResult,text="C\'est un chien",font=fontStyle_Result)
                Chien.pack()
                #displays model accuracy 
            if(self.__var.get()==1 ):
                test_datagen = ImageDataGenerator(rescale=1.0/255.0)
                test_it = test_datagen.flow_from_directory('test/',class_mode='binary', batch_size=64, target_size=(200, 200))
                _,acc1 = model_b.evaluate_generator(test_it, steps=len(test_it), verbose=0)
                
                accurance_VGG_b=tk.Label(master=fenetreResult,text="Le modèle VGG 3 blocs avec augmentation des donnees a une precision de " +' %.3f' % (acc1 * 100.0)+" %",font=fontStyle_Result)
                accurance_VGG_b.pack()
        
	#load modele with transfert learning
        if (self.__CheckVar1.get() == 0 and self.__CheckVar2.get()== 1) :
            img = tf.keras.preprocessing.image.load_img(self.__filename, target_size=(224, 224))
            # convert to array
            img = tf.keras.preprocessing.image.img_to_array(img)
            # reshape into a single sample with 3 channels
            img = img.reshape(1, 224, 224, 3)
            # center pixel data
            img = img.astype('float32')
            img = img - [123.68, 116.779, 103.939]
            # Load the Keras model into a session.
            model_t = tf.keras.models.load_model('CatsVsDogs.h5', custom_objects={'KerasLayer':hub.KerasLayer})
            #model = tf.keras.experimental.load_from_saved_model("CatsVsDogs.h5", custom_objects={'KerasLayer':hub.KerasLayer})
           # predict the class
            result1 = model_t.predict(img)
            #print(result[0,1])
            #Analysis of the prediction
            if (result1[0,0]>=0.125):
                Chat=tk.Label(fenetreResult,text="C\'est un chat",font=fontStyle_Result)
                Chat.pack()
            else:
                Chien=tk.Label(fenetreResult,text="C\'est un chien",font=fontStyle_Result)
                Chien.pack()
                # displays the probability of the prediction 
            if(self.__var.get()==1):
                accurance_tl=tk.Label(master=fenetreResult,text="L\'image prédicte avec le modele par transfert de l'apprentissage a une probabilite d'etre correcte de " +' %.3f' % (result1[0,1] * 100.0)+" %",font=fontStyle_Result)
                accurance_tl.pack()
                if(result1[0,1] < 0.8):
                    non_fiab=tk.Label(master=fenetreResult,text="La prediction est potentiellement fausse ",font=fontStyle_Result)
                    non_fiab.pack()
                else:
                    fiab=tk.Label(master=fenetreResult,text="La prediction est fiable",font=fontStyle_Result)
                    fiab.pack()
                    
                
            #load of two models        
        if (self.__CheckVar1.get() == 1 and self.__CheckVar2.get()== 1) :
            img = tf.keras.preprocessing.image.load_img(self.__filename, target_size=(224, 224))
            # convert to array
            img = tf.keras.preprocessing.image.img_to_array(img)
            # reshape into a single sample with 3 channels
            img = img.reshape(1, 224, 224, 3)
            # center pixel data
            img = img.astype('float32')
            img = img - [123.68, 116.779, 103.939]
            # Load the Keras model into a session.
            model_t = tf.keras.models.load_model('CatsVsDogs.h5', custom_objects={'KerasLayer':hub.KerasLayer})
           # predict the class
            result1 = model_t.predict(img)
            if(result1[0,0]>=0.125):
                Chat=tk.Label(fenetreResult,text="C\'est un chat",font=fontStyle_Result)
                Chat.pack()
            else:
                Chien=tk.Label(fenetreResult,text="C\'est un chien",font=fontStyle_Result)
                Chien.pack()
                #Analysis of the prediction
            if(self.__var.get()==1 ):
                  # displays model 3 blocs and data augmentation 
            
                test_datagen = ImageDataGenerator(rescale=1.0/255.0)
                model_b = tf.keras.models.load_model('final_model_CNN3b_DataAugmentation.h5')
                test_it1 = test_datagen.flow_from_directory('test/',class_mode='binary', batch_size=64, target_size=(200, 200))
                _,acc1 = model_b.evaluate_generator(test_it1, steps=len(test_it1), verbose=0)
                accurance_VGG_b=tk.Label(master=fenetreResult,text="Le modèle VGG 3 blocs avec augmentation des donnees a une precision de " +' %.3f' % (acc1 * 100.0)+" %",font=fontStyle_Result)
                accurance_VGG_b.pack()
                #displays the probability of the prediction
                accurance_tl=tk.Label(master=fenetreResult,text="L\'image prédicte avec le modele par transfert de l'apprentissage a une probabilite d'etre correcte de " +' %.3f' % (result1[0,1] * 100.0)+" %",font=fontStyle_Result)
                accurance_tl.pack()
                
                if(result1[0,1] < 0.8):
                    non_fiab=tk.Label(master=fenetreResult,text="La prediction est potentiellement fausse ",font=fontStyle_Result)
                    non_fiab.pack()
                else:
                    fiab=tk.Label(master=fenetreResult,text="La prediction est fiable",font=fontStyle_Result)
                    fiab.pack()
             
            
                
                
        button_exit = tk.Button(master=fenetreResult, text = "Sortir",bg="red",fg="white", command = fenetreResult.destroy)
        button_exit.pack()
        
        
            
window = MyWindow()
window.mainloop()


