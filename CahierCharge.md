CR de la réunion du 17/04/2020
- Proposition de différents projets(reconnaisance d'images pour détection espèce,modélisation besoin terrain agricole,analyse de lames de prélevement sanguins...)
- Projet retenu la reconnaisance d'images par machine learning pour détection espèces.
- Début des Mooc : Machine Learning


CR de la reunion du 24/04/2020
- Chaque personne du projet a créé un compte git afin de programmer et de contribuer en même temps à l'avancée de celui-ci.
- Récapitulatif de l'avancée des MOOc
- Choix d'une espèce , pour commencer chien/chat

CR de la reunion du 27/04/2020
Objectifs :
- pour vendredi 01/05/2020 
	* Tous les membres du projet doivent avoir fini le mooc machine learning
    * Debut de la partie programmation
- Attribution des taches : 
    * Vivian et Fabien : debut de biblio 
    * Steven communication + CR reunion
    * Clement Lilian et Emeric groupe annexe mais présent pour aider
## Date avancée  
- 29/04/2020 Réalisation d'un graphique de gantt pour planifier le project voir l'avancé
- 30/04/2020 Recherche Bibliographique Introduction
- 12/05/2020 Test de code
- 29/05/2020 Début rédaction introduction
- 12/06/2020 Creation Interface graphique
- 13/06/2020 au 23 juin écriture du rapport et relecture
- 23/06/2020 Rendu rapport écrit version pdf envoyé
- 

## Qui sommes nous ?
Nouys sommes un groupe de trois étudiants du master Bioinformatique de Rennes 1. Dans le cadre de nos études nous avons réalisé ce projet pour la reconaissance de chien et chat.Ce projet nous apermis de nous initier au dep Learning.
Il est à noter que nous sommes pas des professionelle , les codes peuvent bien sur etre ameliorée.



